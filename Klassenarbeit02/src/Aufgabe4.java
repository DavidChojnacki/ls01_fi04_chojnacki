import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
		int zahl = 10;

		System.out.print(romanNumerals(zahl));
	}

	public static char romanNumerals(int zahl) {
		char ergebnis = ' ';
		switch (zahl) {
		case (1):
			ergebnis = 'I';
			break;
		case (5):
			ergebnis = 'V';
			break;
		case (10):
			ergebnis = 'X';
			break;
		case (50):
			ergebnis = 'L';
			break;
		case (100):
			ergebnis = 'C';
			break;
		case (500):
			ergebnis = 'D';
			break;
		case (1000):
			ergebnis = 'M';
			break;
		default:
			System.out.println("?");
			break;
		}
		return ergebnis;

	}

}
