﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rückgabebetrag;

		while (true) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);
			System.out.print("\n\nNeues Ticket\n\n");
		}
	}

	// --------
	// Methoden
	// --------

	// Fahrkartenbestellung Erfassung
	public static double fahrkartenbestellungErfassen() {

		double[] fahrkartenPreis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

		String[] fahrkartenBeschreibung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC", };

		int beschreibungLaenge = fahrkartenBeschreibung.length;
//1. Durch das benutzen von Arrays ist es leichter neue Fahrkarten hinzuzufügen und diese zu verwalten(einfach neue Fahrkarte in gleiche Position bei der Beschreibung und dem Preis hinzufügen)
//3. Mit Arrays ist es schwerer komplizierte Angebote zu verwirklichen. Dafür lassen sich mit Arrays einfach neue Fahrkarten hinzufügen ohne den restlichen Code anzufassen.
		System.out.printf("%-16s%-40s%-16s%n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
		for (int x = 0; x < beschreibungLaenge; x++) {
			System.out.printf("%-16d%-40s%-4.2f€%n", (x + 1), fahrkartenBeschreibung[x], fahrkartenPreis[x]);
		}

		System.out.print("\nWelches Ticket?: ");
		int ticket = tastatur.nextInt();
		double zuZahlenderBetrag = fahrkartenPreis[ticket - 1];
		
		System.out.print("Anzahl der Tickets : ");
		Integer anzahlTicket = tastatur.nextInt();
		if (anzahlTicket > 10) {
			anzahlTicket = 1;
			System.out.println(
					"Es können nicht mehr als 10 Tickets bestellt werden !\n Anzahl der Tickets auf 1 gesetzt !\n");
		} else if (anzahlTicket < 1) {
			anzahlTicket = 1;
			System.out.println(
					"ungültige Eingabe, Anzahl der Tickets kann nicht weniger als 1 sein\n Anzahl der Tickets auf 1 gesetzt !\n");
		}

		return zuZahlenderBetrag * anzahlTicket;
	}

	// Fahrkarten Bezahlung und Rückgabebetrag
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag) - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag);
		return rückgabebetrag;
	}

	// Fahrkarten Ausgabe
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// Rückgeld Ausgabe
	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

}