
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf( "%-12s|", "Fahrenheit" );
		System.out.printf( "%10s\n", "Celsius" );
		System.out.println("----------------------");
		System.out.printf( "%+-12d|" , -20);
		System.out.printf( "%+10.2f\n" , -28.89);
		System.out.printf( "%+-12d|" , -10);
		System.out.printf( "%+10.2f\n" , -23.33);
		System.out.printf( "%+-12d|" , +0);
		System.out.printf( "%+10.2f\n" , -17.78);
		System.out.printf( "%+-12d|" , +20);
		System.out.printf( "%+10.2f\n" , -6.67);
		System.out.printf( "%+-12d|" , +30);
		System.out.printf( "%+10.2f\n" , -1.11);
		
		
	}

}
