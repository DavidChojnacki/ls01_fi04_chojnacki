import java.util.Scanner;

public class PCHaendler {

	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);	
		String artikel = myScanner.next();
	return artikel;		
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);	
		String a = myScanner.next();
	return a;		
		
	}
//	
//	public static double liesDouble(String text) {
//		
//		
//	}
//	
//	public static double berechneGesamtnettopreis(int anzahl, double
//	nettopreis) {
//		
//		
//	}
//	
//	public static double berechneGesamtnettopreis(double nettogesamtpreis,
//	double mwst) {
//		
//		
//	}
//	
//	public static void rechungausgeben(String artikel, int anzahl, double
//	nettogesamtpreis, double bruttogesamtpreis,
//	double mwst) {
//		
//		
//	}


	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("was moechten Sie bestellen?");
		String artikel = liesString();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}